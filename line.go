package line

import (
	"os"
	"bufio"
	"strconv"
)

var reader = bufio.NewReader(os.Stdin)

func Read() (result string) {
	for {
		line, prefix, err := reader.ReadLine()
		if err != nil {
			panic(err)
		}
		result += string(line)
		if !prefix {
			return
		}
	}
}

func ReadNumber() (int, error) {
	line := Read()
	i, err := strconv.ParseInt(line, 10, 32)
	
	return int(i), err
}

